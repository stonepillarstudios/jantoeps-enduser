package com.sps.jvr;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.sps.jvr.utils.JanTools;

import java.util.Calendar;


public class Settings extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        JanTools.setContext(this);

        if(!read("com.sps.jvr.maandagTye").equals("false")){
            ((Button) findViewById(R.id.btnMaandag)).setText("Deaktiveer Maandag Tye vir vandag");

        }

        Calendar c = Calendar.getInstance();

        if (read("com.sps.jvr.vibreer").equals("true")){
            ToggleButton tb = (ToggleButton) findViewById(R.id.tglVibreer);
            tb.toggle();
        }

        if (c.get(Calendar.DAY_OF_WEEK) == 2){
            Button btn = (Button) findViewById(R.id.btnMaandag);
            btn.setText("Aktiveer Maandag Tye vir Vandag\n(Afgeskakel vir Maandag)");
            btn.setEnabled(false);
        }

        if (c.get(Calendar.DAY_OF_WEEK)>= 2 && c.get(Calendar.DAY_OF_WEEK)<= 6)
            findViewById(getResources().getIdentifier("btnSwitch"+translateToAfr(c.get(Calendar.DAY_OF_WEEK)), "id", getPackageName())).setEnabled(false);

        if (!read("com.sps.jvr.switchVakke").equals("false")){
            for (String langdag : JanTools.langdae){
                findViewById(getResources().getIdentifier("btnSwitch"+langdag, "id", getPackageName())).setEnabled(false);
            }
            findViewById(R.id.btnDeRuil).setEnabled(true);
        }

        if (read("com.sps.jvr.sync", "true")){
            ((ToggleButton) findViewById(R.id.tglSync)).toggle();
        }else{
            findViewById(R.id.btnForceSync).setEnabled(false);
            ((Button) findViewById(R.id.btnForceSync)).setText("Sinkroniseer\n(Sink Af)");
            findViewById(R.id.tglWifi).setEnabled(false);
        }

        if(JanTools.readBool("wifi")){
            ((ToggleButton) findViewById(R.id.tglWifi)).toggle();
        }

        if (JanTools.readBool("vakansie") || isNaweek()){
            findViewById(R.id.btnMaandag).setVisibility(View.GONE);
            findViewById(R.id.btnSwitchPeriodes).setVisibility(View.GONE);
            findViewById(R.id.btnDeRuil).setVisibility(View.GONE);
            findViewById(R.id.linLayRuil).setVisibility(View.GONE);
            findViewById(R.id.txtRuil).setVisibility(View.GONE);
            findViewById(R.id.imgRuil).setVisibility(View.GONE);
            findViewById(R.id.txtHideTyeSettings).setVisibility(View.VISIBLE);

            if (isNaweek()){
                ((TextView) findViewById(R.id.txtHideTyeSettings)).setText("(Tye Stellings Afgeskakel vir die Naweek)");
            }
        }

        if (JanTools.readBool("devEnabled")){
            findViewById(R.id.btnDev).setVisibility(View.VISIBLE);
        }
    }

    private boolean isNaweek(){
        Calendar c = Calendar.getInstance();
        return (
                c.get(Calendar.DAY_OF_WEEK)==1 || c.get(Calendar.DAY_OF_WEEK)==7 ||
                (c.get(Calendar.DAY_OF_WEEK)==6 && c.get(Calendar.HOUR_OF_DAY)*60 + c.get(Calendar.MINUTE) >= 845)
        );
    }

    public void onReconfigureClick(View view){
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Herkonfigureer")
                .setMessage("Dit is moontlik dat jy jou stellings kan verloor. Ek belowe niks nie. Is jy seker jy wil voort gaan?")
                .setPositiveButton("Ja", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        write("com.sps.jvr.configDone", "false");
                        JanTools.makeToast("Sukses.");
                        gotoActivity(FirstRun.class);
                    }

                })
                .setNegativeButton("Nee", null)
                .show();
    }

    public void onMaandagTyeClicked(View view){
        Button btnMaandag = (Button) findViewById(R.id.btnMaandag);
        if (btnMaandag.getText().equals("Aktiveer Maandag Tye vir vandag")){
            btnMaandag.setText("Deaktiveer Maandag Tye vir vandag");
            Calendar c = Calendar.getInstance();
            write("com.sps.jvr.maandagTye", c.get(Calendar.DAY_OF_WEEK) + "");
            makeToast("Maandag Tye geaktiveer vir vandag", "short");
            gotoActivity(MainScreen.class);
        }else if(btnMaandag.getText().equals("Deaktiveer Maandag Tye vir vandag")){
            btnMaandag.setText("Aktiveer Maandag Tye vir vandag");
            write("com.sps.jvr.maandagTye", "false");
            makeToast("Maandag Tye gedeaktiveer.", "short");
            gotoActivity(MainScreen.class);
        }
    }

    public void onDevSettingsClick(View view){ gotoActivity(DevSettings.class); }

    public void onForceSyncClick(View view){
        gotoActivity(FirstRun.class);
    }

    public void onTerugClick(View view){
        gotoActivity(MainScreen.class);
    }

    public void onBackPressed(){
        onTerugClick(findViewById(R.id.btnTerug));
    }

    public void onSwitchMaandagClick(View view){
        Calendar c = Calendar.getInstance();
        write("com.sps.jvr.switchVakkeDag", translateToAfr(c.get(Calendar.DAY_OF_WEEK)));
        write("com.sps.jvr.switchVakke", "Maandag");
        makeToast("Maandag se vakke vir vandag geaktiveer", "short");
        gotoActivity(MainScreen.class);
    }

    public void onSwitchDinsdagClick(View view){
        Calendar c = Calendar.getInstance();
        write("com.sps.jvr.switchVakkeDag", translateToAfr(c.get(Calendar.DAY_OF_WEEK)));
        write("com.sps.jvr.switchVakke", "Dinsdag");
        makeToast("Dinsdag se vakke vir vandag geaktiveer", "short");
        gotoActivity(MainScreen.class);
    }

    public void onSwitchWoensdagClick(View view){
        Calendar c = Calendar.getInstance();
        write("com.sps.jvr.switchVakkeDag", translateToAfr(c.get(Calendar.DAY_OF_WEEK)));
        write("com.sps.jvr.switchVakke", "Woensdag");
        makeToast("Woensdag se vakke vir vandag geaktiveer", "short");
        gotoActivity(MainScreen.class);
    }

    public void onSwitchDonderdagClick(View view){
        Calendar c = Calendar.getInstance();
        write("com.sps.jvr.switchVakkeDag", translateToAfr(c.get(Calendar.DAY_OF_WEEK)));
        write("com.sps.jvr.switchVakke", "Donderdag");
        makeToast("Donderdag se vakke vir vandag geaktiveer", "short");
        gotoActivity(MainScreen.class);
    }

    public void onSwitchVrydagClick(View view){
        Calendar c = Calendar.getInstance();
        write("com.sps.jvr.switchVakkeDag", translateToAfr(c.get(Calendar.DAY_OF_WEEK)));
        write("com.sps.jvr.switchVakke", "Vrydag");
        makeToast("Vrydag se vakke vir vandag geaktiveer", "short");
        gotoActivity(MainScreen.class);
    }

    public void onDeRuilClick(View view){
        write("com.sps.jvr.switchVakke", "false");
        write("com.sps.jvr.switchVakkeDag", "false");
        makeToast("Vakke ruil gedeaktiveer.", "short");
        gotoActivity(MainScreen.class);
    }

    public void onAboutClick(View view){
        gotoActivity(About.class);
    }

    public void onSyncClick(View view) {
        boolean checked = ((ToggleButton) view).isChecked();
        if (checked){
            write("com.sps.jvr.sync", "true");
            ((Button) findViewById(R.id.btnForceSync)).setText("Sinkroniseer Nou");
            findViewById(R.id.btnForceSync).setEnabled(true);
            findViewById(R.id.tglWifi).setEnabled(true);
        }else{
            write("com.sps.jvr.sync", "false");
            ((Button) findViewById(R.id.btnForceSync)).setText("Sinkroniseer Nou\n(Sink Af)");
            findViewById(R.id.btnForceSync).setEnabled(false);
            findViewById(R.id.tglWifi).setEnabled(false);
        }

        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Restart Toeps")
                .setMessage("Jy moet die toeps restart om die stelling te verander.")
                .setPositiveButton("Restart", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        gotoActivity(FirstRun.class);
                    }
                })
                .setNegativeButton("Later", null)
                .show();
    }

    public void onPeriodeSwitchClick(View view){
        gotoActivity(SwitchPeriodes.class);
    }

    public void onNaSkoolClick(View view) {
        gotoActivity(LeerderConfig3.class);
    }

    public void onVibreerClick(View view) {
        ToggleButton tgl = (ToggleButton) view;
        if (tgl.isChecked()){
            new AlertDialog.Builder(this)
                    .setTitle("Let Wel")
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setMessage("Let wel dat hierdie funksie nog nie 100% werk nie. As jou foon eienaardige goed begin doen, sit hierdie af.")
                    .setNeutralButton("Ek verstaan", null)
                    .show();
            write("com.sps.jvr.vibreer", "true");
        }else{
            write("com.sps.jvr.vibreer", "false");
        }
        makeToast("Vibrasie geskakel.", "short");
    }

    public void onWifiClick(View view){
        boolean checked = ((ToggleButton) view).isChecked();
        JanTools.write("wifi", checked + "");
        JanTools.makeToast("Stelling verander.");
    }

    //-------------------------------------------------Other Methods

    //-------------------------------------------------Tool Methods

    public String read(String key){
        SharedPreferences sharedPref = this.getSharedPreferences("com.sps.jvr.info", this.MODE_PRIVATE);
        return sharedPref.getString(key, "0");
    }

    public boolean read(String in, String cmp){
        return read(in).equals(cmp);
    }

    public void gotoActivity(Class where){
        Intent intent = new Intent(this, where);
        startActivity(intent);
        finish();
    }

    public void makeToast(String say, String length){
        Toast toast;
        if (length.equals("long")) {
            toast = Toast.makeText(this, say, Toast.LENGTH_LONG);
        }else{
            toast = Toast.makeText(this, say, Toast.LENGTH_SHORT);
        }
        toast.show();
    }

    public void write(String key, String val){
        SharedPreferences sharedPref = this.getSharedPreferences("com.sps.jvr.info", this.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(key, val);
        editor.commit();
    }

    public String translateToAfr(int input){
        switch (input){
            case 1:
                return "Sondag";
            case 2:
                return "Maandag";
            case 3:
                return "Dinsdag";
            case 4:
                return "Woensdag";
            case 5:
                return "Donderdag";
            case 6:
                return "Vrydag";
            case 7:
                return "Saterdag";
        }
        return "Error";
    }

    public void onProfielClick(View view) {
        gotoActivity(Profile.class);
    }

    private int smileyCount = 0;
    public void onSmileyClick(View view) {//todo
        smileyCount++;
        if (smileyCount >= 5){
            if (JanTools.readBool("devEnabled")){
                JanTools.makeToast("Dev stellings reeds aan");
            }else {
                (findViewById(R.id.btnDev)).setVisibility(View.VISIBLE);
                JanTools.write("devEnabled", "true");
                JanTools.makeToast("Dev stellings aangeskakel");
            }
        }
    }
}