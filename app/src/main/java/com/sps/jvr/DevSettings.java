package com.sps.jvr;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.sps.jvr.utils.JanTools;

import java.util.Arrays;

public class DevSettings extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dev_settings);
        JanTools.setContext(this);

        if (!JanTools.readBool("dnsCustomNaSkool")){
            findViewById(R.id.btnResetDnsCustomNaskool).setEnabled(false);
        }
        if (!JanTools.readBool("dnsLeeProfiel")){
            findViewById(R.id.btnResetDnsLeeProfiel).setEnabled(false);
        }
    }

    public void onTerugClick(View view){
        gotoActivity(Settings.class);
    }

    public void onVarsUpdateFileClick(View view){
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_info)
                .setTitle("Vars: Update File")
                .setMessage(
                        "weird_tye: " + JanTools.read("com.sps.jvr.weird") + "\n"
                        + "eksamens: " + JanTools.read("com.sps.jvr.eksamen") + "\n"
                        + "vakansie: " + JanTools.read("com.sps.jvr.vakansie") + "\n"
                        + "latest_version: " + JanTools.read("com.sps.jvr.version") + "\n"
                        //afkondigings retrieved and processed on MainScreens
                )
                .setNeutralButton("OK", null)
                .show();
    }

    public void onBackPressed() {
        onTerugClick(findViewById(R.id.btnTerug));
    }

    public void gotoActivity(Class where){
        Intent intent = new Intent(this, where);
        startActivity(intent);
        finish();
    }

    public void setToggle(String key, View tgl){
        if (read(key, "true")){
            ((ToggleButton) tgl).toggle();
        }
    }

    public void makeToast(String s){
        if (s.substring(s.length() - 2).equals("/l")) {
            Toast.makeText(this, s.substring(0, s.length() - 2), Toast.LENGTH_LONG).show();
        }else{
            Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
        }
    }

    public void write(String key, String val){
        SharedPreferences sharedPref = this.getSharedPreferences("com.sps.jvr.info", this.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(key, val);
        editor.apply();
    }

    public String read(String key) {
        SharedPreferences sharedPref = this.getSharedPreferences("com.sps.jvr.info", this.MODE_PRIVATE);
        return sharedPref.getString(key, "0");
    }

    public boolean read(String in, String cmp){
        return read(in).equals(cmp);
    }

    public void onCrashClick(View view) {
        String[] fail = new String[69];
        Arrays.fill(fail, "=P");
        System.out.println(fail[420]);
    }

    public void onResetdnsCustomNaskoolClick(View view) {
        JanTools.write("dnsCustomNaSkool", "false");
        view.setEnabled(false);
    }

    public void onResetdnsLeeProfielClick(View view) {
        JanTools.write("dnsLeeProfiel", "false");
        view.setEnabled(false);
    }

    public void onHideDevClick(View view) {
        JanTools.write("devEnabled", "false");
        gotoActivity(Settings.class);
    }
}
