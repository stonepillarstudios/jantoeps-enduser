package com.sps.jvr;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Looper;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import com.sps.jvr.utils.JanTools;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;
import org.jsoup.Jsoup;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.TimeUnit;

public class Feedback extends Activity {

    static class Konst{
        static String timeServer = "http://rauten.co.za/time/";
        static String feedbackServer = "rauten.co.za";
        static String ftpUser = "apps@rauten.co.za";
        static String ftpPword = "6GNygKSGP8Nc";
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(null);
        setContentView(R.layout.activity_feedback);
        JanTools.setContext(this);

        String[] tipes = {
                "Probleem",
                "Idee",
                "Iets Anders"
        };

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.custom_spinner_item, tipes);
        ((Spinner) findViewById(R.id.spinnerTipe)).setAdapter(adapter);

        random = JanTools.randInt(0, 999_999_999);

        localFile = new File(this.getCacheDir() + "/" + random + ".txt");

        realTimeTask.execute(Konst.timeServer);
    }

    public int random = 123456;
    public ProgressDialog dialog;
    public getRealTime realTimeTask = new getRealTime();
    public File localFile = null;
    public long realTime = 0;

    public void onUploadClick(View view) {
        dialog = new ProgressDialog(this);
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setIcon(android.R.drawable.ic_dialog_alert);
        dialog.setTitle("Besig om op te laai...");
        dialog.setMessage("Besig om dinge reg te kry...");
        dialog.show();

        EditText edtTerug = (EditText) findViewById(R.id.edtTerugvoer);
        if (edtTerug.getText().toString().isEmpty()){
            JanTools.makeToast("Jy moet iets by jou terugvoer sit.");
            dialog.dismiss();
            return;
        }

        try{
            realTime = realTimeTask.get(11, TimeUnit.SECONDS);
        }catch (Exception e){
            e.printStackTrace();
            JanTools.makeToast("Iets het fout gegaan. Probeer weer.");
            dialog.dismiss();
            finish();
        }

        if(realTime == 0){
            JanTools.makeToast("Iets het fout gegaan. Probeer weer.");
            dialog.dismiss();
            finish();
            return;
        }

        long verskil = Math.abs(realTime - Long.parseLong(JanTools.read("terugvoerTimestamp")));
        if(verskil <= 60*60*24*1000){
            verskil = 60*60*24*1000 - verskil;
            int ure = Math.round((verskil/1000) / (60*60));
            int mins = Math.round(( (verskil/1000) % (60*60) ) / 60);
            String wag = "Jy kan net een keer per dag terugvoer gee.\nWag nog " + ure + " ure";
            if (mins != 0) wag += " en " + mins + " minute./l";
            JanTools.makeToast(wag);
            dialog.dismiss();
            return;
        }

        String naam = ((EditText) findViewById(R.id.edtNaam)).getText().toString();
        String epos = ((EditText) findViewById(R.id.edtEpos)).getText().toString();
        String selfoon = ((EditText) findViewById(R.id.edtSelfoon)).getText().toString();
        String terugvoer = edtTerug.getText().toString();
        String tipe = ((Spinner) findViewById(R.id.spinnerTipe)).getSelectedItem().toString();

        try{
            localFile.createNewFile();

            FileWriter fw = new FileWriter(localFile);
            BufferedWriter writer = new BufferedWriter(fw);

            JanTools.writeLine("Naam: " + naam, writer);
            JanTools.writeLine("E-pos: " + epos, writer);
            JanTools.writeLine("Selfoon: " + selfoon, writer);
            JanTools.writeLine("Terugvoer:", writer);
            writer.write(terugvoer); //no newline after last
            writer.flush();

            fw.close();
            writer.close();

            dialog.setMessage("Koppel aan wolkbediener...");

            new UploadFile().execute(tipe);

        }catch(Exception e){
            JanTools.makeToast("Flater: " + e.getMessage());
            e.printStackTrace();
            dialog.dismiss();
        }
    }

    class UploadFile extends AsyncTask<String, Void, Void> {

        public Void doInBackground(String... params){

            Looper.prepare();

            String remoteDir = "/jantoeps/feedback/" + params[0] + "/" + random + ".txt";
            String localDir = Feedback.this.getCacheDir() + "/" + random + ".txt";

            try {
                FTPClient ftp = new FTPClient();
                ftp.connect(Konst.feedbackServer);
                if (!ftp.login(Konst.ftpUser, Konst.ftpPword)){
                    ftp.logout();
                    throw new IOException("Kon nie wolkbediener bereik nie.");
                }
                int reply = ftp.getReplyCode();
                if (!FTPReply.isPositiveCompletion(reply)){
                    ftp.disconnect();
                    throw new IOException("Wolkbediener is van lyn af: " + reply);
                }
                ftp.enterLocalPassiveMode();
                InputStream input = new FileInputStream(localDir);

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        dialog.setMessage("Stuur terugvoer...");
                    }
                });

                ftp.storeFile(remoteDir, input);

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        dialog.setMessage("Sukses. Sluit af...");
                    }
                });

                input.close();
                ftp.logout();
                ftp.disconnect();

                localFile.delete();

                JanTools.write("terugvoerTimestamp", realTime + "");

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        dialog.dismiss();
                        JanTools.makeToast("Oplaai sukses!");
                    }
                });

            }catch(final Exception e){
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						dialog.dismiss();
						JanTools.makeToast("Flater: " + e.getMessage());
					}
				});
                e.printStackTrace();
            }

            return null;
        }

    }

    class getRealTime extends AsyncTask<String, Void, Long>{

        public Long doInBackground(String... params){

            Looper.prepare();

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    dialog = new ProgressDialog(Feedback.this);
                    dialog.setIcon(android.R.drawable.ic_dialog_alert);
                    dialog.setTitle("Koppel aan bediener...");
                    dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    dialog.setMessage("Net een sekonde...");
                    dialog.setCancelable(false);
                    dialog.show();
                }
            });


            try {
                String timeMillis = Jsoup.connect(params[0]).timeout(10_000).get().select("body").get(0).text();

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        dialog.dismiss();
                        JanTools.makeToast("Bediener aanlyn.");
                    }
                });

                return Long.parseLong(timeMillis);

            }catch (Exception e){
                e.printStackTrace();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        dialog.dismiss();
                        new AlertDialog.Builder(Feedback.this)
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .setTitle("Kon nie aan wolkbediener koppel nie.")
                                .setMessage("Of jy of die bediener is van lyn af. Maak seker jy het internet toegang en probeer weer. As hierdie aanhou gebeur, stuur eerder vir my 'n e-pos:\nsps@rauten.co.za")
                                .setNeutralButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        finish();
                                    }
                                })
                                .setCancelable(false)
                                .show();

                    }
                });
            }

            return null;

        }

    }

    public void onTerugClick(View view) {
        finish();
    }

    public void onBackPressed(){
        finish();
    }

}
