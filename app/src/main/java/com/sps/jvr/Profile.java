package com.sps.jvr;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import com.sps.jvr.utils.JanTools;

import java.util.Calendar;

public class Profile extends Activity {

    private EditText edtNaam;
    private Spinner spinGeslag;
    private int gebJaar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        JanTools.setContext(this);

        String[] geslagte = {
                "Kies jou geslag",
                "Vroulik",
                "Manlik",
                "Ander"
        };
        Spinner spinGeslag = (Spinner) findViewById(R.id.spinGeslag);
        spinGeslag.setAdapter(new ArrayAdapter<String>(this, R.layout.custom_spinner_item, geslagte));

        if (!JanTools.read("naam", "0")){
            ((EditText) findViewById(R.id.edtNaam)).setText(JanTools.read("naam"));
        }
        if (!JanTools.read("geslag", "0")){
            int pos = 0;
            while (!JanTools.read("geslag", spinGeslag.getItemAtPosition(pos).toString()) && pos < spinGeslag.getAdapter().getCount()){
                pos++;
            }
            if (pos < spinGeslag.getAdapter().getCount()){
                spinGeslag.setSelection(pos);
            }
        }
        if (!JanTools.read("geboorteJaar", "0")){
            ((EditText) findViewById(R.id.edtGeboorteJaar)).setText(JanTools.read("geboorteJaar"));
        }
    }

    public void onButWhyClick(View view) {
        new AlertDialog.Builder(this)
                .setTitle("Maar hoekom?")
                .setIcon(android.R.drawable.ic_menu_help)
                .setMessage("Hierdie data word gebruik om akkurate, relevante advertensies te genereer. Google het persoonlik vir my gesê hulle doen niks anders daarmee nie ;)")
                .setNeutralButton(android.R.string.ok, null)
                .show();
    }

    public void onTerugClick(View view) {
        edtNaam = (EditText) findViewById(R.id.edtNaam);
        spinGeslag = (Spinner) findViewById(R.id.spinGeslag);
        gebJaar = Integer.parseInt(((EditText) findViewById(R.id.edtGeboorteJaar)).getText().toString());
        int currYear = Calendar.getInstance().get(Calendar.YEAR);

        if (edtNaam.getText().toString().isEmpty()){
            JanTools.makeToast("Naam is leeg");
        }else if (spinGeslag.getSelectedItemPosition() == 0){
            JanTools.makeToast("Kies jou geslag");
        }else if (gebJaar > currYear || gebJaar < 1900){
            JanTools.makeToast("Geboortejaar onmoontlik");
        }else{
            if (currYear-gebJaar > 100){
                new AlertDialog.Builder(this)
                        .setTitle("Ouderdom lyk shady")
                        .setMessage("Is jy regitg " + (currYear-gebJaar) + " jaar oud?")
                        .setIcon(android.R.drawable.stat_notify_error)
                        .setCancelable(false)
                        .setPositiveButton("Ja", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                save();
                            }
                        })
                        .setNegativeButton("Nee", null)
                        .show();
            }else{
                save();
            }
        }
    }

    private void save(){
        JanTools.write("naam", edtNaam.getText().toString());
        JanTools.write("geslag", spinGeslag.getSelectedItem().toString());
        JanTools.write("geboorteJaar", gebJaar + "");
        JanTools.makeToast("Stellings gestoor");
        gotoActivity(MainScreen.class);
    }

    private void gotoActivity(Class where){
        Intent intent = new Intent(this, where);
        startActivity(intent);
        finish();
    }

    public void onKanselleerClick(View view) {
        gotoActivity(Settings.class);
    }

    public void onBackPressed(){
        onKanselleerClick(findViewById(R.id.btnKanselleer));
    }
}
