package com.sps.jvr;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.Toast;

import com.sps.jvr.utils.JanTools;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class StartScreen extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_screen);
    }

    public void gotoActivity(Class where){
        Intent intent = new Intent(this, where);
        startActivity(intent);
        finish();
    }

    public void onOnderwyserClick(View view){
        gotoActivity(OnderwyserConfig1.class);
    }

    public void onLeerderClick(View view){
        File backup = new File("/storage/emulated/0/JVR_Backup/com.sps.jvr.info.xml");
        if(backup.exists()){
            new AlertDialog.Builder(this)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle("Backup Gevind")
                    .setMessage("Ek sien jy het 'n backup van jou settings. Wil jy dit toepas?\n<LW: Restart Required>")
                    .setPositiveButton("Ja", new DialogInterface.OnClickListener()
                    {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            restoreSettings();
                            finish();
                        }

                    })
                    .setNegativeButton("Nee", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            nee();
                        }
                    })
                    .show();
        }else {
            gotoActivity(LeerderConfig1.class);
        }
    }

    public void nee(){
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Verwyder Backup")
                .setMessage("Wil jy die backup verwyder?")
                .setPositiveButton("Ja", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        File backup = new File("/storage/emulated/0/JVR_Backup/com.sps.jvr.info.xml");
                        if (backup.delete()) {
                            makeToast("Sukses.", "short");
                        } else {
                            makeToast("Mislukking: Kon nie leêr verwyder nie.", "short");
                        }

                        if (read("com.sps.jvr.configDone").equals("true")) {
                            gotoActivity(MainScreen.class);
                        } else {
                            gotoActivity(LeerderConfig1.class);
                        }
                    }

                })
                .setNegativeButton("Nee", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (read("com.sps.jvr.configDone").equals("true")) {
                            gotoActivity(MainScreen.class);
                        } else {
                            gotoActivity(LeerderConfig1.class);
                        }
                    }
                })
                .show();
    }

    public String read(String key){
        SharedPreferences sharedPref = this.getSharedPreferences("com.sps.jvr.info", this.MODE_PRIVATE);
        return sharedPref.getString(key, "0");
    }

    public void onSkipSetupClick(View view){
        JanTools.makeToast("Hierdie funksie is verwyder vanaf 2016 =P/l");
    }

    public void makeToast(String say, String length){
        Toast toast;
        if (length.equals("long")) {
            toast = Toast.makeText(this, say, Toast.LENGTH_LONG);
        }else{
            toast = Toast.makeText(this, say, Toast.LENGTH_SHORT);
        }
        toast.show();
    }

    public void write(String key, String val){
        SharedPreferences sharedPref = this.getSharedPreferences("com.sps.jvr.info", this.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(key, val);
        editor.commit();
    }

    public void copy(File src, File dst) throws IOException {
        InputStream in = new FileInputStream(src);
        OutputStream out = new FileOutputStream(dst);

        // Transfer bytes from in to out
        byte[] buf = new byte[1024];
        int len;
        while ((len = in.read(buf)) > 0) {
            out.write(buf, 0, len);
        }
        in.close();
        out.close();
    }

    public String getSharedPrefsDir(){
        return this.getFilesDir().getPath().substring(0, this.getFilesDir().getPath().length() - 5) + "shared_prefs";
    }

    public void restoreSettings(){
        boolean sukses = true;
        File del = new File(getSharedPrefsDir() + "/com.sps.jvr.info.xml");
        if(del.exists()){
            sukses = del.delete();
        }
        File get = new File(Environment.getExternalStorageDirectory().getPath() + "/JVR_Backup/com.sps.jvr.info.xml");
        try {
            copy(get, del);
        } catch (IOException e) {
            sukses = false;
            System.out.println(e);
        }

        makeToast("Sukses: " + sukses + "\nRestart nou asseblief jou foon.", "long");
    }

}
