package com.sps.jvr;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.sps.jvr.utils.JanTools;

public class OnderwyserConfig2 extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_onderwyser_config2);
        setTitle("Onderwyser Config 2");
        JanTools.setContext(this);

        initSpinners();
        setSpinners();
    }

    public void gotoActivity(Class where){
        Intent intent = new Intent(this, where);
        startActivity(intent);
        finish();
    }

    public void initSpinners(){
        int commas = 0;
        String vakkeString = JanTools.read("com.sps.jvr.gegeweVakke");
        for (int i = 0; i < vakkeString.length(); i++){
            if (vakkeString.substring(i, i+1).equals(",")) commas++;
        }
        String vakke[] = new String[commas + 2];
        vakke[0] = "[vak]";
        int prevI = 0;
        int j = 1;
        for (int i = 0; i < vakkeString.length(); i++) {
            if (vakkeString.substring(i, i + 1).equals(",")) {
                vakke[j] = vakkeString.substring(prevI, i);
                prevI = i + 1;
                j++;
            }
        }
        vakke[vakke.length - 1] = "Af";

        commas = 0;
        String gradeString = JanTools.read("com.sps.jvr.grade");
        for (int i = 0; i < gradeString.length(); i++){
            if (gradeString.substring(i, i+1).equals(",")) commas++;
        }
        String grade[] = new String[commas + 1];
        grade[0] = "[gr]";
        prevI = 0;
        j = 1;
        for (int i = 0; i < gradeString.length(); i++) {
            if (gradeString.substring(i, i + 1).equals(",")) {
                grade[j] = gradeString.substring(prevI, i);
                prevI = i + 1;
                j++;
            }
        }

        String seksies[] = {
                "[klas]",
                "H",
                "J",
                "V",
                "R"
        };


        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.custom_spinner_item, vakke);
        //adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        for (int i = 1; i <=8; i++){
            int Id = getResources().getIdentifier("spinnerP" + i, "id", getPackageName());
            ((Spinner) findViewById(Id)).setAdapter(adapter);
            ((Spinner) findViewById(Id)).setOnItemSelectedListener(new MyOnItemSelectedListener());

        }

        adapter = new ArrayAdapter<>(this, R.layout.custom_spinner_item, grade);
        //adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        for (int i = 1; i <=8; i++){
            int Id = getResources().getIdentifier("spinnerGraadP" + i, "id", getPackageName());
            ((Spinner) findViewById(Id)).setAdapter(adapter);
            ((Spinner) findViewById(Id)).setOnItemSelectedListener(new MyOnItemSelectedListener());
        }

        adapter = new ArrayAdapter<>(this, R.layout.custom_spinner_item, seksies);
        //adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        for (int i = 1; i <=8; i++){
            int Id = getResources().getIdentifier("spinnerSeksieP" + i, "id", getPackageName());
            ((Spinner) findViewById(Id)).setAdapter(adapter);
            ((Spinner) findViewById(Id)).setOnItemSelectedListener(new MyOnItemSelectedListener());
        }

        spinnerInitDone = true;
    }

    public boolean checkSpinnersForDefault(){
        for (int i = 1; i <= 8; i++){
            int Id = getResources().getIdentifier("spinnerP" + i, "id", getPackageName());
            Spinner tempSpinner = (Spinner) findViewById(Id);
            if (tempSpinner.getSelectedItem().toString().equals("[vak]")){
                JanTools.makeToast("Gee 'n waarde vir die vak by periode " + i + "/l");
                return false;
            }
        }
        for (int i = 1; i <= 8; i++){
            Spinner tempSpinner = (Spinner) findViewById(
                    getResources().getIdentifier("spinnerGraadP" + i, "id", getPackageName())
            );
            if (tempSpinner.getSelectedItem().toString().equals("[gr]") && tempSpinner.getVisibility() == View.VISIBLE){
                JanTools.makeToast("Gee 'n waarde vir die graad by periode " + i + "/l");
                return false;
            }
        }
        for (int i = 1; i <= 8; i++){
            Spinner tempSpinner = (Spinner) findViewById(
                    getResources().getIdentifier("spinnerSeksieP" + i, "id", getPackageName())
            );
            if (tempSpinner.getSelectedItem().toString().equals("[klas]") && tempSpinner.getVisibility() == View.VISIBLE){
                JanTools.makeToast("Gee 'n waarde vir die klas by periode " + i + "/l");
                return false;
            }
        }
        return true;
    }

    public void saveSpinners(){
        String dag;
        switch (dayEditing){
            case 1:
                dag = "maandag";
                break;
            case 2:
                dag = "dinsdag";
                break;
            case 3:
                dag = "woensdag";
                break;
            case 4:
                dag = "donderdag";
                break;
            case 5:
                dag = "vrydag";
                break;
            default:
                dag = "maandag";
                break;
        }
        for (int i = 1; i <= 8; i++){
            Spinner vakSpinner = (Spinner) findViewById(
                    getResources().getIdentifier("spinnerP" + i, "id", getPackageName())
            );
            Spinner graadSpinner = (Spinner) findViewById(
                    getResources().getIdentifier("spinnerGraadP" + i, "id", getPackageName())
            );
            Spinner seksieSpinner = (Spinner) findViewById(
                    getResources().getIdentifier("spinnerSeksieP" + i, "id", getPackageName())
            );

            String write = vakSpinner.getSelectedItem().toString();
            if (graadSpinner.getVisibility() == View.VISIBLE){
                write += " " + graadSpinner.getSelectedItem().toString();
            }
            if (seksieSpinner.getVisibility() == View.VISIBLE){
                write += seksieSpinner.getSelectedItem().toString();
            }

            JanTools.write("com.sps.jvr." + dag + "P" + i, write);
        }
    }

    public void setSpinners(){
        String dag;
        switch (dayEditing){
            case 1:
                dag = "maandag";
                break;
            case 2:
                dag = "dinsdag";
                break;
            case 3:
                dag = "woensdag";
                break;
            case 4:
                dag = "donderdag";
                break;
            case 5:
                dag = "vrydag";
                break;
            default:
                dag = "maandag";
                break;
        }

        for (int i = 1; i <= 8; i++){
            Spinner vakSpinner = (Spinner) findViewById(
                    getResources().getIdentifier("spinnerP" + i, "id", getPackageName())
            );
            String temp = JanTools.read("com.sps.jvr." + dag + "P" + i);
            System.out.println(temp);

            if (!temp.equals("0")) {
                int spacePos = temp.lastIndexOf(" ");
                //if (temp.length() > 6) {
                //    if (temp.substring(0, 6).equals("Wisk G")) spacePos = 6;
                //}

                String readVak;
                if (spacePos == -1){
                    readVak = temp;
                }else {
                    readVak = temp.substring(0, spacePos);
                }

                int j = 0;
                while (!readVak.equals(vakSpinner.getAdapter().getItem(j)) && j < vakSpinner.getAdapter().getCount()) {
                    j++;
                    if (j == vakSpinner.getAdapter().getCount()){
                        vakSpinner.setSelection(0);
                        spinnerItemSelected(i + "");
                        return;
                    }
                }
                vakSpinner.setSelection(j);

            }else{
                vakSpinner.setSelection(0);
            }
            spinnerItemSelected(i + "");
        }

        for (int i = 1; i <= 8; i++){
            Spinner graadSpinner = (Spinner) findViewById(
                    getResources().getIdentifier("spinnerGraadP" + i, "id", getPackageName())
            );
            Spinner seksieSpinner = (Spinner) findViewById(
                    getResources().getIdentifier("spinnerSeksieP" + i, "id", getPackageName())
            );
            String temp = JanTools.read("com.sps.jvr." + dag + "P" + i);

            if (!temp.equals("0")) {
                int spacePos = temp.lastIndexOf(" ");

                if (spacePos != -1) {
                    String readGraad = temp.substring(spacePos + 1, temp.length() - 1);

                    if (!readGraad.equals("1")) {
                        String readSeksie = temp.substring(temp.length() - 1);

                        int j = 0;
                        while (!readGraad.equals(graadSpinner.getAdapter().getItem(j)) && j < graadSpinner.getAdapter().getCount()) {
                            j++;
                            if (j == graadSpinner.getAdapter().getCount()){
                                graadSpinner.setSelection(0);
                                return;
                            }
                        }
                        graadSpinner.setSelection(j);

                        j = 0;
                        while (!readSeksie.equals(seksieSpinner.getAdapter().getItem(j)) && j < seksieSpinner.getAdapter().getCount()) {
                            j++;
                            if (j == seksieSpinner.getAdapter().getCount()){
                                seksieSpinner.setSelection(0);
                                return;
                            }
                        }
                        seksieSpinner.setSelection(j);

                    }else{
                        seksieSpinner.setSelection(0);

                        readGraad = temp.substring(spacePos + 1);

                        int j = 0;
                        while (!readGraad.equals(graadSpinner.getAdapter().getItem(j)) && j < graadSpinner.getAdapter().getCount()) {
                            j++;
                        }
                        if (readGraad.equals(graadSpinner.getAdapter().getItem(j))) {
                            graadSpinner.setSelection(j);
                        } else {
                            graadSpinner.setSelection(0);
                        }
                    }
                }else{
                    seksieSpinner.setSelection(0);
                    graadSpinner.setSelection(0);
                }
            }else{
                seksieSpinner.setSelection(0);
                graadSpinner.setSelection(0);
            }
        }
    }

    public static boolean isInteger(String s) {
        if(s.isEmpty()) return false;
        for(int i = 0; i < s.length(); i++) {
            if(i == 0 && s.charAt(i) == '-') {
                if(s.length() == 1) return false;
                else continue;
            }
            if(Character.digit(s.charAt(i), 10) < 0) return false; // ... , 10 = radix
        }
        return true;
    }

    public void clearSpinners(){
        for (int i = 1; i <= 8; i++){
            ((Spinner) findViewById(
                    getResources().getIdentifier("spinnerP" + i, "id", getPackageName())
            )).setSelection(0);
            ((Spinner) findViewById(
                    getResources().getIdentifier("spinnerGraadP" + i, "id", getPackageName())
            )).setSelection(0);
            ((Spinner) findViewById(
                    getResources().getIdentifier("spinnerSeksieP" + i, "id", getPackageName())
            )).setSelection(0);
        }
    }

    public void onBackPressed(){
        onTerugClick(findViewById(R.id.btnTerug));
    }

    public void onTerugClick(View view){
        dayEditing--;
        if (dayEditing == 0) {
            gotoActivity(OnderwyserConfig1.class);
        }else{
            setDag();
            clearSpinners();
            setSpinners();
        }
    }

    public void onVolgendeClick(View view){
        if (checkSpinnersForDefault()){
            //all tests passed
            saveSpinners();
            JanTools.makeToast("Periodes gestoor.");
            dayEditing++;
            if (dayEditing == 6){
                JanTools.makeToast("Konfigurasie klaar.");
                JanTools.write("com.sps.jvr.configDone", "true");
                gotoActivity(MainScreen.class);
            }else {
                setDag();
                clearSpinners();
                setSpinners();
            }
        }
    }

    public void setDag(){
        TextView txt = ((TextView) findViewById(R.id.txtDag));
        String set;
        switch (dayEditing){
            case 1:
                set = "Maandag";
                break;
            case 2:
                set = "Dinsdag";
                break;
            case 3:
                set = "Woensdag";
                break;
            case 4:
                set = "Donderdag";
                break;
            case 5:
                set = "Vrydag";
                break;
            default:
                set = "Maandag";
                break;
        }
        txt.setText(set);
    }

    //public
    public int dayEditing = 1;
    public boolean spinnerInitDone = false;
    //end

    public boolean isJuniorOnlyVak(String vak){
        String juniorOnlyVakke[] = {
                "NW",
                "EBW",
                "Teg",
        };
        for (String aJuniorOnlyVakke : juniorOnlyVakke) {
            if (vak.equals(aJuniorOnlyVakke)) return true;
        }
        return false;
    }

    public boolean isMultiOnlyVak(String vak){
        String multiOnlyVakke[] = {
                "Eng Home",
                "Eng Ad",
                "LO",
                "MBK",
                "Afr"
        };
        for (String iterate : multiOnlyVakke) {
            if (vak.equals(iterate)) return true;
        }
        return false;
    }

    public void spinnerItemSelected(String pLine){
        if (spinnerInitDone) {

            pLine = pLine.substring(pLine.length() - 1);

            Spinner vakSpinner = ((Spinner) findViewById(
                    getResources().getIdentifier("spinnerP" + pLine, "id", getPackageName())
            ));
            Spinner graadSpinner = ((Spinner) findViewById(
                    getResources().getIdentifier("spinnerGraadP" + pLine, "id", getPackageName())
            ));
            Spinner seksieSpinner = ((Spinner) findViewById(
                    getResources().getIdentifier("spinnerSeksieP" + pLine, "id", getPackageName())
            ));

            int graad;
            if (graadSpinner.getSelectedItem().toString().equals("[gr]")){
                graad = 0;
            }else {
                graad = Integer.parseInt(graadSpinner.getSelectedItem().toString());
            }
            String vak = vakSpinner.getSelectedItem().toString();

            graadSpinner.setVisibility(View.VISIBLE);
            seksieSpinner.setVisibility(View.VISIBLE);

            if (vak.equals("Af")){
                seksieSpinner.setVisibility(View.INVISIBLE);
                graadSpinner.setVisibility(View.INVISIBLE);
            }else if (!isJuniorOnlyVak(vak) && graad >= 10 && !isMultiOnlyVak(vak)){
                seksieSpinner.setVisibility(View.INVISIBLE);
            }
            if(isJuniorOnlyVak(vak) && graad >= 10){
                JanTools.makeToast(vak + " is net vir Juniors.");
                graadSpinner.setSelection(0);
            }

        }

    }

    public class MyOnItemSelectedListener implements AdapterView.OnItemSelectedListener {

        public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
            String pLine = String.valueOf(getResources().getResourceEntryName(parent.getId()));
            spinnerItemSelected(pLine);
        }



        public void onNothingSelected(AdapterView parent) {
            // Do nothing.
        }
    }

    public void onMissingClick(View view) {
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Ontbreking")
                .setMessage("Is daar 'n vak nie hier nie? Kort daar 'n naskoolse aktiwiteit? Is die periodes verkeerd?\nAs daar enigiets fout is, laat weet my onmiddelik!")
                .setPositiveButton("Gee Terugvoer", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(OnderwyserConfig2.this, Feedback.class);
                        startActivity(intent);
                        //purposefully not finish()ing so that user can return to this activity as it was
                    }
                })
                .setNegativeButton("Nvm", null)
                .show();
    }

}

